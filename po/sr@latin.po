# Serbian translation of libgtop
# Courtesy of Prevod.org team (http://prevod.org/) -- 2003—2017.
# This file is distributed under the same license as the libgtop package.
# Branko Ivanović <popeye@one.ekof.bg.ac.yu>
# Danilo Šegan <danilo@gnome.org>, 2005-03-06
# Miroslav Nikolić <miroslavnikolic@rocketmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: libgtop\n"
"Report-Msgid-Bugs-To: https://bugzilla.gnome.org/enter_bug.cgi?product=libgto"
"p&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2017-04-07 11:45+0000\n"
"PO-Revision-Date: 2017-08-14 21:53+0200\n"
"Last-Translator: Miroslav Nikolić <miroslavnikolic@rocketmail.com>\n"
"Language-Team: srpski <gnome-sr@googlegroups.org>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1? 3 : n%10==1 && n%100!=11 ? 0 : "
"n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Project-Style: gnome\n"

#: lib/read.c:49
#, c-format
msgid "read %d byte"
msgid_plural "read %d bytes"
msgstr[0] "pročitan je %d bajt"
msgstr[1] "pročitana su %d bajta"
msgstr[2] "pročitano je %d bajtova"
msgstr[3] "pročitan je jedan bajt"

#: lib/read_data.c:49
msgid "read data size"
msgstr "veličina pročitanih podataka"

#: lib/read_data.c:66
#, c-format
msgid "read %lu byte of data"
msgid_plural "read %lu bytes of data"
msgstr[0] "pročitan je %lu bajt podataka"
msgstr[1] "pročitana su %lu bajta podataka"
msgstr[2] "pročitano je %lu bajtova podataka"
msgstr[3] "pročitan je jedan bajt podataka"

#: lib/write.c:49
#, c-format
msgid "wrote %d byte"
msgid_plural "wrote %d bytes"
msgstr[0] "upisan je %d bajt"
msgstr[1] "upisana su %d bajta"
msgstr[2] "upisano je %d bajtova"
msgstr[3] "upisan je jedan bajt"

#: src/daemon/gnuserv.c:456
msgid "Enable debugging"
msgstr "Uključuje ispravljanje grešaka"

#: src/daemon/gnuserv.c:458
msgid "Enable verbose output"
msgstr "Uključuje detaljni prikaz"

#: src/daemon/gnuserv.c:460
#| msgid "Don't fork into background"
msgid "Don’t fork into background"
msgstr "Ne iscepljuje u pozadini"

#: src/daemon/gnuserv.c:462
msgid "Invoked from inetd"
msgstr "Pokrenuto iz „inetd“-a"

#: src/daemon/gnuserv.c:498
#, c-format
#| msgid ""
#| "Run '%s --help' to see a full list of available command line options.\n"
msgid "Run “%s --help” to see a full list of available command line options.\n"
msgstr ""
"Pokrenite „%s --help“ da vidite čitav spisak dostupnih opcija linije naredbi."
"\n"

#: sysdeps/osf1/siglist.c:27 sysdeps/sun4/siglist.c:27
msgid "Hangup"
msgstr "Obustavi"

#: sysdeps/osf1/siglist.c:28 sysdeps/sun4/siglist.c:28
msgid "Interrupt"
msgstr "Prekini"

#: sysdeps/osf1/siglist.c:29 sysdeps/sun4/siglist.c:29
msgid "Quit"
msgstr "Izađi"

#: sysdeps/osf1/siglist.c:30 sysdeps/sun4/siglist.c:30
msgid "Illegal instruction"
msgstr "Neispravna instrukcija"

#: sysdeps/osf1/siglist.c:31 sysdeps/sun4/siglist.c:31
msgid "Trace trap"
msgstr "Prati zamku"

#: sysdeps/osf1/siglist.c:32 sysdeps/sun4/siglist.c:32
msgid "Abort"
msgstr "Odustani"

#: sysdeps/osf1/siglist.c:33 sysdeps/sun4/siglist.c:33
msgid "EMT error"
msgstr "EMT greška"

#: sysdeps/osf1/siglist.c:34 sysdeps/sun4/siglist.c:34
msgid "Floating-point exception"
msgstr "Izuzetak sa pokretnim zarezom"

#: sysdeps/osf1/siglist.c:35 sysdeps/sun4/siglist.c:35
msgid "Kill"
msgstr "Ubij"

#: sysdeps/osf1/siglist.c:36 sysdeps/sun4/siglist.c:36
msgid "Bus error"
msgstr "Greška u magistrali"

#: sysdeps/osf1/siglist.c:37 sysdeps/sun4/siglist.c:37
msgid "Segmentation violation"
msgstr "Pogrešan pristup memoriji (segv)"

#: sysdeps/osf1/siglist.c:38 sysdeps/sun4/siglist.c:38
msgid "Bad argument to system call"
msgstr "Loš argument sistemskog poziva"

#: sysdeps/osf1/siglist.c:39 sysdeps/sun4/siglist.c:39
msgid "Broken pipe"
msgstr "Oštećena spojka"

#: sysdeps/osf1/siglist.c:40 sysdeps/sun4/siglist.c:40
msgid "Alarm clock"
msgstr "Budilnik"

#: sysdeps/osf1/siglist.c:41 sysdeps/sun4/siglist.c:41
msgid "Termination"
msgstr "Okončanje"

#: sysdeps/osf1/siglist.c:42 sysdeps/sun4/siglist.c:42
msgid "Urgent condition on socket"
msgstr "Kritično stanje utičnice"

#: sysdeps/osf1/siglist.c:43 sysdeps/sun4/siglist.c:43
msgid "Stop"
msgstr "Stani"

#: sysdeps/osf1/siglist.c:44 sysdeps/sun4/siglist.c:44
msgid "Keyboard stop"
msgstr "Zaustavi tastaturu"

#: sysdeps/osf1/siglist.c:45 sysdeps/sun4/siglist.c:45
msgid "Continue"
msgstr "Nastavi"

#: sysdeps/osf1/siglist.c:46 sysdeps/sun4/siglist.c:46
msgid "Child status has changed"
msgstr "Stanje poroda se promenilo"

#: sysdeps/osf1/siglist.c:47 sysdeps/sun4/siglist.c:47
msgid "Background read from tty"
msgstr "Pozadinsko čitanje sa „tty“"

#: sysdeps/osf1/siglist.c:48 sysdeps/sun4/siglist.c:48
msgid "Background write to tty"
msgstr "Pozadinsko pisanje na „tty“"

#: sysdeps/osf1/siglist.c:49 sysdeps/sun4/siglist.c:49
msgid "I/O now possible"
msgstr "U/I je sada moguć"

#: sysdeps/osf1/siglist.c:50 sysdeps/sun4/siglist.c:50
msgid "CPU limit exceeded"
msgstr "Prekoračeno je ograničenje procesora"

#: sysdeps/osf1/siglist.c:51 sysdeps/sun4/siglist.c:51
msgid "File size limit exceeded"
msgstr "Prekoračeno je ograničenje veličine datoteke"

#: sysdeps/osf1/siglist.c:52 sysdeps/sun4/siglist.c:52
msgid "Virtual alarm clock"
msgstr "Virtuelni budilnik"

#: sysdeps/osf1/siglist.c:53 sysdeps/sun4/siglist.c:53
msgid "Profiling alarm clock"
msgstr "Profilisanje budilnika"

#: sysdeps/osf1/siglist.c:54 sysdeps/sun4/siglist.c:54
msgid "Window size change"
msgstr "Promena veličine prozora"

#: sysdeps/osf1/siglist.c:55 sysdeps/sun4/siglist.c:55
msgid "Information request"
msgstr "Zahtev za informacijama"

#: sysdeps/osf1/siglist.c:56 sysdeps/sun4/siglist.c:56
msgid "User defined signal 1"
msgstr "Korisnički definisan signal 1"

#: sysdeps/osf1/siglist.c:57 sysdeps/sun4/siglist.c:57
msgid "User defined signal 2"
msgstr "Korisnički definisan signal 2"

#~ msgid "DEBUG"
#~ msgstr "ISPRAVLJANJE GREŠAKA"

#~ msgid "VERBOSE"
#~ msgstr "DETALJNO"

#~ msgid "NO-DAEMON"
#~ msgstr "BEZ-USLUGE"

#~ msgid "INETD"
#~ msgstr "INETD"
